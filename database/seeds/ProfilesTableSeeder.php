<?php

use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profiles')->insert([
        	'nama'=> 'aleg',
        	'alamat'=> 'alge',
        	'nope'=>'022525'

        ]);
        DB::table('profiles')->insert([
        	'nama'=> 'bebas',
        	'alamat'=> 'palsu',
        	'nope'=>'0248855'

        ]);

    }
}
